from scrapy.spider import BaseSpider
import lxml.html as html 


class TinyDealSpider(BaseSpider):
	name = "china"
	allowed_domains = ["tinydeal.com"]
	start_urls = ["http://www.tinydeal.com/products_new.html"]


	def parse(self, response):
		doc = html.document_fromstring(response.body)
		arr = doc.cssselect('img')
		for img in arr:
			print img.attrib.keys()